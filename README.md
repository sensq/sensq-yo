# README #
### What is this repository for? ###

This is a Yo for Sensq API with Meteor Example Hello app.

A sample app built, Politics Dashboard demo can be viewed real time demo at http://sensq.meteor.com 

If you wanted to start developing atop of it, feel free to clone the code near https://bitbucket.org/sensq/sensq-yo
 
### How do I get set up? ###

1. clone the repo https://bitbucket.org/sensq/sensq-yo (by default under sensq1/ folder)
2. get API key near https://sensq.com/api 
3. create a settings.json file under a new folder, said NewFolder1/ with the format:


```
#!javascript

{
  "sensqApiKey": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
  "twitterApp": {
  	"consumer": {
        "key": "xxxxxxxxxxxxxxxxxxx",
        "secret": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    },
    "access_token": {
        "key": "xxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "secret": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    }
  },
  "twitterLogin": {
  	"consumer": {
        "key": "xxxxxxxxxxxxxxxxxxx",
        "secret": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    }
  },
  "public": {
  	
  }
}
```


4. Meteor Run with Settings 

```
#!javascript

> cd sensq-yo
> meteor --settings ../NewFolder1/settings.json
```

Feel free to ignore this GUI if not applicable, just for example purpose.
 
The API call is simple, the sample script:

A. server/sensq.js


```
#!javascript

Meteor.methods({
  Sensq: function(usercontent) {
    var sensqArray = HTTP.post( "https://sensq.com/api/v1", { 
     data: {
       // storing api key as Meteor Environment Settings
       "api_key": Meteor.settings.sensqApiKey, 
       "content": usercontent
     } 
    });

    return sensqArray.data;
  }
});
```


B. server/twitter.js


```
#!javascript

Meteor.call('Sensq', tweet.text, function(error, result) {
			      	Tweets.insert({tweet: userTweet, idd: result.idd, cat: result.cat}, function(error){  
                    	if(error)
                        	console.log(error);
                	});
			    }); 
```



REST SDK
     
```
#!javascript

curl -H "Content-Type: application/json" 
     -X POST -d '{
       "api_key": "{{apiKey}}", 
       "content":"{{content.content}}"}' 
     https://sensq.com/api/v1
```