if (Meteor.isClient) {

  Template.hello.helpers({
    insight: function () {
      var realtimeInsight = Session.get("realtimeInsight");
      return realtimeInsight;
    }
  });

  Template.hello.events({
    'click button': function () {

      var usercontent = "#guachiman7d El opositor venezolano, partido socialista unido @dcabellor fue asesinato por ordenes de y ahorapodemos bienes :-D.  proyectovzla fue asesinato por ordenes de @NicolasMaduro y bolivarianismo :-(";

      // Calling Sensq Method with usercontent variable
      Meteor.call('Sensq', usercontent, function(error, result) {
        Session.set("realtimeInsight", result);
      });
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {

  });

  Meteor.methods({
    Sensq: function(usercontent) {
      var sensqArray = HTTP.post( "https://sensq.com/api/v1", { 
       data: {
         // storing api key as Meteor Environment Settings
         "api_key": Meteor.settings.sensqApiKey, 
         "content": usercontent
       } 
      });

      return sensqArray.data;
    }
  });
}
